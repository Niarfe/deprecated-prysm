from setuptools import setup

setup(name='prysm',
      version='0.4',
      description='Lightweight text coloring for clis',
      url='https://Niarfe@bitbucket.org/Niarfe/prysm.git',
      author='Efrain Olivares',
      author_email='efrain.olivares@gmail.com',
      license='MIT',
      packages=['prysm'],
      zip_safe=False)
 
